
package CodeGen;

import scanner.PascalToken;
import parser.SymbolTable;
import syntaxtree.*;


/**
 *This Class will create the code for the MIPS assembly language.
 * @author Henry Constantino
 */
public class CodeGeneration {
    
   /**The t register always starts at 0 only 8 t registers **/
    private int currentTRegister = 0;
    /** Counter for the while loop branches. **/
    private int loopCounter = 0;
    /** Counter for the if branches. **/
    private int ifCounter = 0;
    
    /**
     * This method takes the input of the tree to start generating code
     * @param root
     * @return the actual code for MIPS emulator
     */
    public String writeCodeForRoot(ProgramNode root) 
    {
        StringBuilder code = new StringBuilder();
        code.append( ".data\n");
        
        //Handles all the global declarations.
        for (VariableNode n : root.getVariables().getVars()){
            String zeros = "0";
            code.append(n.getName()).append(":   .word   ").append(zeros).append("\n");
        }
        code.append("\n\n");
        code.append( ".text\n");
        code.append( "main:\n");
        
        //Handles all of the statements in the program.
        CompoundStatementNode state = root.getMain();
        for (StatementNode sNode : state.getStatements()){
            code.append(writeCode(sNode,"$t" + this.currentTRegister));
        }
        
        code.append( "syscall\n");
        return( code.toString());   //Prints the MIPS code
    }
    
    /**
     * This is to write the code if the node is an instance of a type of StatemntNode
     * @param node
     * @param reg
     * @return returns the instance of code
     */
    public String writeCode(StatementNode node, String reg)
    {
    	String nodeCode = null;
        //Checks for an assignment Statement node
    	if (node instanceof AssignmentStatementNode){
            nodeCode = writeCode((AssignmentStatementNode)node, reg);
        //Checks for an While Statement node
    	}else if (node instanceof WhileStatementNode)
    	{
            nodeCode = writeCode((WhileStatementNode)node, reg);
        //Checks for an If Statement node
    	}else if (node instanceof IfStatementNode)
    	{    		
            nodeCode = writeCode((IfStatementNode)node, reg);
        //Checks for an Compound Statement node
    	}else if (node instanceof CompoundStatementNode)
    	{
            nodeCode = writeCode((CompoundStatementNode)node, reg);
    	}
    	return nodeCode;
    }
    
    /**
     * This is to write the code if the node is an instance of a type of Compound
     * @param node
     * @param reg
     * @return
     */
    public String writeCode(CompoundStatementNode node, String reg)
    {
    	String nodeCode = "";
        //Gets all the statements 
    	for (StatementNode n : node.getStatements()){
            nodeCode += writeCode(n, reg);
    	}
    	return nodeCode;
    }
    
    /**
     * This should check if the node passed in is of type while
     * @param node
     * @param reg
     * @return the while code for MIPS
     */
    public String writeCode(WhileStatementNode node, String reg)
    {
    	String nodeCode = "";
    	
    	ExpressionNode exp = node.getExpression();
    	nodeCode += writeBranchCode((OperationNode)exp, reg, "endloop" + this.loopCounter);
    	nodeCode += "j      loop" + this.loopCounter + "\n";
    	
    	nodeCode += "\nloop" + this.loopCounter + ":\n";
    	StatementNode state = node.getStatement();
    	nodeCode += writeCode(state,reg);
    	nodeCode += writeBranchCode((OperationNode)exp, reg, "endloop" + this.loopCounter);
    	nodeCode += "j      loop" + this.loopCounter + "\n";
    	nodeCode += "\nendloop" + this.loopCounter + ":\n";
    	
    	this.loopCounter++;
    	return nodeCode;
    }
    
    /**
     * This should check if the node passed in is of type if 
     * @param node
     * @param reg
     * @return the if code for MIPS
     */
    public String writeCode(IfStatementNode node, String reg)
    {
    	String nodeCode = "";
    	ExpressionNode exp = node.getExpresion();
    	nodeCode += writeBranchCode((OperationNode)exp,reg,"Else" + this.ifCounter);
    	
    	nodeCode += writeCode(node.getStatements_1(), reg);
    	nodeCode += "j      Endif" + + this.ifCounter + "\n";
    	nodeCode += "Else" + this.ifCounter + ":\n";
    	nodeCode += writeCode(node.getStatements_2(), reg);
    	nodeCode += "Endif" + this.ifCounter + ":\n";
    	
    	this.ifCounter++;
    	return nodeCode;
    }
    
    /**
     * 
     * @param node
     * @param reg
     * @return
     */
    public String writeCode(AssignmentStatementNode node, String reg)
    {
    	String nodeCode = null;    	
    	nodeCode = writeCode(node.getExpression(), reg);
    	nodeCode += "sw     " + reg + ",   " + node.getLvalue().getName() + "\n";
    	
    	return nodeCode;
    }
    
    /**
     * This is to write the code if the node is an instance of a type of Expression
     * @param node
     * @param reg
     * @return
     */
    public String writeCode(ExpressionNode node, String reg)
    {
    	String nodeCode = null;
    	
        if( node instanceof OperationNode) {
            nodeCode = writeCode( (OperationNode)node, reg);
        }else if( node instanceof ValueNode) {
            nodeCode = writeCode( (ValueNode)node, reg);
        }else if( node instanceof VariableNode) {
            nodeCode = writeCode( (VariableNode)node, reg);
        }
        
    	return nodeCode;
    }
    
    /**
     * This is to write the code if the node is an instance of a type of Variable
     * @param node
     * @param reg
     * @return
     */
    public String writeCode(VariableNode node, String reg)
    {
    	String nodeCode = "lw     " + reg + ",   " + node.getName();
    	return nodeCode + "\n";
    }
    
    /**
     * This is to write the code if the node is an instance of a type of Operation
     * @param opNode
     * @param resultRegister
     * @return
     */
    public String writeCode( OperationNode opNode, String resultRegister)
    {
        String code;
        ExpressionNode left = opNode.getLeft();
        String leftRegister = "$t" + currentTRegister++;
        code = writeCode( left, leftRegister);
        ExpressionNode right = opNode.getRight();
        String rightRegister = "$t" + currentTRegister++;
        code += writeCode( right, rightRegister);
        PascalToken kindOfOp = opNode.getOperation();
        
       if(kindOfOp == PascalToken.PLUS)
        {
            code+= "add    "+resultRegister+",      "+leftRegister+
                   ",       "+rightRegister   + "\n";
        }
        if(kindOfOp == PascalToken.MINUS)
        {
            code += "sub    "+resultRegister+",       "+leftRegister+
                    ",      " + rightRegister+"\n";
        }
        if(kindOfOp == PascalToken.MULTIPLY)
        {
            code += "mult   "+leftRegister+",       "+rightRegister + "\n";
            code += "mflo   " + resultRegister;
        }
        if(kindOfOp == PascalToken.DIVIDE)
        {
            code += "div    " +leftRegister+",      "+rightRegister +"\n";
            code += "mflo   "+ resultRegister;
        }
        if(kindOfOp == PascalToken.AND)
        {
            code += "and    "+leftRegister+",       "+rightRegister+"\n";
            code += "$d " + resultRegister;
        }
        if(kindOfOp == PascalToken.OR)
        {
            code += "or    "+leftRegister+",        "+rightRegister+"\n";
            code += "$d " + resultRegister;
        }
             
        this.currentTRegister -= 2;
 
        return(code);
    }
    
    /**
     * This is to write the code if the node is an instance of a type of Operation
     * @param opNode
     * @param resultRegister
     * @param tag
     * @return
     */
    public String writeBranchCode(OperationNode opNode, String resultRegister, String tag)
    {
        String code;
        ExpressionNode left = opNode.getLeft();
        String leftRegister = "$t" + currentTRegister++;
        code = writeCode( left, leftRegister);
        ExpressionNode right = opNode.getRight();
        String rightRegister = "$t" + currentTRegister++;
        code += writeCode( right, rightRegister);
        PascalToken kindOfOp = opNode.getOperation();
                

        if( kindOfOp == PascalToken.EQUALS)   	
        {
            code += "bne     " 
                    + leftRegister + ",   " 
                    + rightRegister + "," 
                    + tag + "\n";
        }
        if( kindOfOp == PascalToken.LESSTHAN)
        {
            code += "bge     " 
                    + leftRegister + ",   " 
                    + rightRegister + ", " 
                    + tag + "\n";
        }
        if( kindOfOp == PascalToken.LESSTHANEQUAL)
        {
            code += "bgt     " 
                    + leftRegister + ",   " 
                    + rightRegister + ", " 
                    + tag + "\n";
        }
        if( kindOfOp == PascalToken.GREATERTHAN)
        {
            code += "ble     " 
                    + leftRegister + ",   " 
                    + rightRegister + ", " 
                    + tag + "\n";
        }
        if (kindOfOp == PascalToken.GREATERTHANEQUAL)
        {
            code += "blt     " 
                    + leftRegister + ",   " 
                    + rightRegister + ", " 
                    + tag + "\n";
        }
        if( kindOfOp == PascalToken.NOTEQUAL)
        {
            code += "beq     " 
                    + leftRegister + ",   " 
                    + rightRegister + ", " 
                    + tag + "\n";

        }
        
        this.currentTRegister -= 2;
        
        return code;
    }
    
    /**
     * This is to write the code if the node is an instance of a type of Value
     * @param valNode
     * @param resultRegister
     * @return
     */
    public String writeCode( ValueNode valNode, String resultRegister)
    {
        String value = valNode.getAttribute();
        String code = "addi   " + resultRegister + ",   $zero, " + value + "\n";
        return( code);
    }
}
