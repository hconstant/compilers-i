/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package parser;

/**
 *This is the used to identify if the variable is a procedure, array, variable, 
 * etc
 * @author Henry Constantino
 */
public enum IdInfoToken {
    VARIABLE_NAME,PROGRAM_NAME,FUNCTION_NAME,PROCEDURE_NAME,
    ARRAY_NAME
}
