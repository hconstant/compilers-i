package parser;

import CodeGen.CodeGeneration;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import syntaxtree.ProgramNode;


/**
 * Tests the Pascal Parser.
 * Uses the first argument to the program on the command line
 * as the name of the file to be read.
 * @author Henry Constantino
 */
public class Executable {
    
    public static void main(String[] args) {
        
        String inpName = "";
        String outName = "";
		
	if (args.length == 2)
	{
            inpName = args[0];
            outName = args[1];
	}else if (args.length == 1)
	{
            inpName = args[0];
            outName = inpName + ".out.txt";
	}else
	{
            System.out.println("ERROR, no input file specified!");
            System.exit(0);
	}
                
        SymbolTable id = new SymbolTable();
        PascalParser parser = new PascalParser(inpName, id);
        ProgramNode program = parser.program(); 

        System.out.println("***End of Parsing***");
        System.out.println("--------------------------");
        System.out.println("***Symbol Table***");
        System.out.println("--------------------------");
        System.out.println(id);
        System.out.println("***End of Symbol Table***");
        
        System.out.println("--------------------------");
        System.out.println("***Parser Syntax Tree****");
        System.out.println("--------------------------");
        System.out.println(program.indentedToString(0));
        System.out.println("***End of Parser Syntax Tree****");
        
        // Build up the code and print it out:
        CodeGeneration cg = new CodeGeneration( );
        String code = cg.writeCodeForRoot(program);
        System.out.println("The code should be:");
        //writeFile(outName,code);
        System.out.println( code);
 
    }
    /**
    * Writes the mips assembly out to a file.
    * @param outName - The name of the file being written to.
    * @param output - The mips that will be written to file.
    */
    public static void writeFile(String outName, String output)
    {
	try
            {
                File file = new File(outName);
			 
		if (!file.exists()) {
                    file.createNewFile();
		}
	
                FileWriter write = new FileWriter(file.getAbsoluteFile());
		BufferedWriter buff = new BufferedWriter(write);
		buff.write(output);
		buff.close();
			
	}catch(IOException e)
	{
                e.printStackTrace();
	}

    }
}
