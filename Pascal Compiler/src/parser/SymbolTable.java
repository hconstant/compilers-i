/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import java.util.Hashtable;

/**
 *
 * @author Henry Constantino
 */

public class SymbolTable {

    private Hashtable<String, Info> myTable = new Hashtable<String, Info>();

    /**
     * Adds a variable ID to the hashtable and stored in a class to hold its
     * name and data type.
     *
     * @param name
     * @param kind
     */
    public void add(String name, IdInfoToken kind) {

        Info valueInfo = new Info();    //Creates class to store data type
        valueInfo.symbol = name;        //stores name
        valueInfo.tokenKind = kind;     //stores kind
        myTable.put(name, valueInfo);   //stored in hashtable
    }

    /**
     * Returns the token ID kind of the variable.
     *
     * @param name
     * @return TokenID
     */
    public IdInfoToken getKind(String name) {
        Info kind = myTable.get(name);   //Gets the token kind from info class
        if (kind != null) {
            return kind.getIdToken();
        } else {
            return null;
        }
    }

    /**
     * Checks to see if the ID is in the hashtable.
     *
     * @param name
     * @return true or false
     */
    public boolean isContained(String name) {
        boolean answear = myTable.containsKey(name);
        return answear;
    }

    /**
     * Returns printed symbol table
     * @return Symbol Table
     */
    @Override
    public String toString() {
        StringBuilder answer = new StringBuilder("Symbol Table");
        answer.append(myTable);
        return answer.toString();
    }

    /**
     * A class that stores the data attributes of the variable ID. This class
     * can be expanded to add more attributes.
     */
    private class Info {

        String symbol;
        IdInfoToken tokenKind;

        public String getSymbol() {
            return symbol;
        }

        public IdInfoToken getIdToken() {
            return tokenKind;
        }

        @Override
        public String toString() {
            String answerInfo = symbol + " " + tokenKind;
            return answerInfo;
        }

    }
}
