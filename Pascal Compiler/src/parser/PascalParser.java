package parser;
//Imported classes

import scanner.PascalScanner;
import scanner.PascalToken;
import scanner.LookupTable;
import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;
import syntaxtree.AssignmentStatementNode;
import syntaxtree.CompoundStatementNode;
import syntaxtree.DeclarationsNode;
import syntaxtree.ExpressionNode;
import syntaxtree.IfStatementNode;
import syntaxtree.OperationNode;
import syntaxtree.ProgramNode;
import syntaxtree.StatementNode;
import syntaxtree.SubProgramDeclarationsNode;
import syntaxtree.ValueNode;
import syntaxtree.VariableNode;
import syntaxtree.WhileStatementNode;

/**
 * A parser for the Pascal language (part of it). This parser will check to see
 * if an input file contains Pascal code. Not all of the Pascal language can be
 * detected in this parser since not all has been added.
 *
 * @author Henry Constantino
 */
public class PascalParser {

    /**
     * The scanner to supply tokens from the input file.
     */
    final PascalScanner scanner;

    /**
     * The current token for one token look ahead.
     */
    private PascalToken currentToken;

    //Creates a symbol table for the variable IDs
    final SymbolTable mySymbolTable;

    //Creates an array to hold IDs so the parser can later on 
    //check if its a variable or Array[].
    final ArrayList<String> idVerification = new ArrayList();

    //Created an array list of variable nodes
    final ArrayList<VariableNode> varNode = new ArrayList();

    /**
     * Creates an Pascal parser to parse the named file.
     *
     * @param filename The name of the file to be parsed.
     * @param idTable Symbol Table for the variable Id
     */
    public PascalParser(String filename, SymbolTable idTable) {
        File input = new File(filename);
        Hashtable symbols = new LookupTable();
        scanner = new PascalScanner(input, symbols);
        mySymbolTable = idTable;
        scanner.nextToken();
        // presume this succeeds
        currentToken = scanner.getToken();
        //program(); //Runs the whole parser when ParcalParser is object created.
    }

    /**
     * Creates the header of the program file. This method will parse the file
     * from the start to the end. When the parsing is complete this method 
     * returns a complete syntax tree of the program. 
     * @return ProgramNode()
     */
    public ProgramNode program() {
        System.out.println("program");
        
        //Creates a program node
        ProgramNode answer = new ProgramNode();
        
        match(PascalToken.PROGRAM);
        //Adds program ID to the symbol table
        mySymbolTable.add(scanner.getAttribute(), IdInfoToken.PROGRAM_NAME);
        //Sets the name of the program in the node
        answer.setName(scanner.getAttribute());
        
        match(PascalToken.ID);
        match(PascalToken.SEMICOLON);
        
        //The declarations method is called to parse all the variables into an
        //arraylist and stores them into to the declarations node. 
        answer.setVariables(declarations());
        
        answer.setFunctions(new SubProgramDeclarationsNode());  //test case
        //Check for subPrograms of procedure methods.
        subprogram_declarations();
        //Calls the compound statements method and stores all the compound 
        //statement nodes in the main of program node
        answer.setMain(compound_statement()); 
        //Checks for end of program. 
        match(PascalToken.PERIOD);
        return answer;
        //End of parsing
    }

    /**
     * Checks to see if there is if there are variable IDs in the 
     * variable parameter.
     */
    private void identifier_list() {
        System.out.println("identifier_list");
        //A variable node is created
        VariableNode node = new VariableNode();
        
        //Adds variable ID to the symbol table
        mySymbolTable.add(scanner.getAttribute(), IdInfoToken.VARIABLE_NAME);
        //The varaiable node is being named
        node.setName(scanner.getAttribute()); 
        //The varaible node is being stored into the ArrayList. 
        varNode.add(node);
        
        //stores the string to validate its data type later on in type method.
        idVerification.add(scanner.getAttribute());

        match(PascalToken.ID);
        if (currentToken == PascalToken.COMMA) {
            match(PascalToken.COMMA);
            //Checks for another variable.
            identifier_list();
        }

    }

    /**
     * This is where a statement would be placed. The begin statement of the
     * main program block is where the program execution begins. However, the
     * end statement indicating the end of the main program is followed by a
     * full stop (.) instead of semicolon (;)
     * @return 
     */
    private CompoundStatementNode compound_statement() {

        //Creates a compound statement node 
        CompoundStatementNode answer = new CompoundStatementNode();
        System.out.println("compound_statement");

        match(PascalToken.BEGIN);
        //Adds all the instance of statement node in the compound statement 
        //array list. Optinal statement passes an array list.
        answer.addAllArrayList(optional_statements());
        match(PascalToken.END);
        
        return answer;
    }

    /**
     * Its optional to have a statement in the compound statement. If there is
     * nothing the parser will just continue on. This method will also return 
     * an array list of instance of statement nodes.
     * @return 
     */
    private ArrayList<StatementNode> optional_statements() {
        
        System.out.println("optional_statements");
        //Will hold all the instance of statement nodes
        ArrayList<StatementNode> answer = new ArrayList();
        
        if (currentToken == PascalToken.IF
               ||currentToken == PascalToken.WHILE 
               ||currentToken == PascalToken.ID
               ||currentToken == PascalToken.BEGIN
               ||currentToken == PascalToken.READ
               ||currentToken == PascalToken.WRITE) {
            //sends down the array list as an argument
            statement_list(answer);
        }    
        return answer;
    }

    /**
     * Checks the structure of statement. statement | statement ; statement_list
     * This method will also return an array list of statement nodes
     * @param answer
     * @return 
     */
    private ArrayList<StatementNode> statement_list(ArrayList<StatementNode> answer) {
        //Creates a statement node. 
        StatementNode node = null;
        System.out.println("statement_list");

        //Sends the statement node to statement method.
        //This is done because the node can be an assignment, while, if, or array
        //The method can also return null and statement nodes do not print null. 
        node = statement(node);
        if(node != null){
            answer.add(node);
        }

        if (currentToken == PascalToken.SEMICOLON) {
            match(PascalToken.SEMICOLON);
            //Will send a statement node again to check for more statements in the 
            //pascal program
            statement_list(answer);
        }
        return answer;
    }

    /**
     * Parses through to check the type of statement. Checks for While, if,
     * variable-expression, etc.
     * @param answer
     * @return 
     */
    private StatementNode statement(StatementNode answer) {
        System.out.println("statement");

        if (currentToken == PascalToken.ID) {
            //Checks the Id type for variable or procedure statement kind.
            IdInfoToken kind = mySymbolTable.getKind(scanner.getAttribute());
            //Creates an assignments statements
            AssignmentStatementNode node = new AssignmentStatementNode();
            
            if (kind == IdInfoToken.VARIABLE_NAME) {
                node.setLvalue(variable()); //Sets the variable node
                match(PascalToken.ASSIGNOP);
                node.setExpression(expression());   //set the expression node
            } else if (kind == IdInfoToken.PROCEDURE_NAME) {
                procedure_statement();
            } else {
                System.out.println("Error in Statement. Statement kind " + kind);
                undeclaredId(scanner.getAttribute());
                error();
            }
            answer = node;

        } else if (currentToken == PascalToken.BEGIN) {
            compound_statement();
        } else if (currentToken == PascalToken.IF) {
            //Creates an if statement node.
            IfStatementNode node = new IfStatementNode();
            match(PascalToken.IF);
            //Sets the expression node of the if statement
            node.setExpression(expression());
            match(PascalToken.THEN);
            //Sets the statements for the if
            node.addIfStatement(statement(node));
            match(PascalToken.ELSE);
            //sets the statements for the else
            node.addElseStatements(statement(node));
            answer = node;  //sets the if statemetns node to be returned 
        } else if (currentToken == PascalToken.WHILE) {
            //Creates a While node
            WhileStatementNode node = new WhileStatementNode();
            match(PascalToken.WHILE);
            node.setExpression(expression()); //Sets the expression 
            match(PascalToken.DO);
            node.addWhileStatement(statement(node));//Adds all the statements
            answer = node;//Set the while node in statement node
        }
        //----------------------------------------//
        //read and write will be treated as Tokens//
        //----------------------------------------//
        else if (currentToken == PascalToken.READ) {
            match(PascalToken.READ);//match READ
            match(PascalToken.LEFT);
            match(PascalToken.ID);//
            match(PascalToken.RIGHT);
        } 
        else if (currentToken == PascalToken.WRITE) {
            match(PascalToken.WRITE);//match write
            match(PascalToken.LEFT);
            expression();
            match(PascalToken.RIGHT);
        }
        else {
            System.out.println("Error in statement. Saw " + currentToken);
            error();
        }
        return answer;
    }

    /**
     * Parses for an ID or ID(expression). Currently not used for the Pascal
     * Scanner
     */
    private void procedure_statement() {

        System.out.println("procedure_statement");

        match(PascalToken.ID);
        if (currentToken == PascalToken.LEFT) {
            match(PascalToken.LEFT);
            expression_list();
            match(PascalToken.RIGHT);
        }
    }

    /**
     * Parses to check the expression structure. expression | expression,
     * expression_list.
     */
    private void expression_list() {

        System.out.println("expression_list");

        expression();
        if (currentToken == PascalToken.COMMA) {
            match(PascalToken.COMMA);
            expression_list();
        }
    }

    /**
     * Parses to check if its just a simple expression or an expression with a
     * relop. If a relop is present the symbol is looked up and matched then
     * parses to check the next part in the expression.
     * @return 
     */
    private ExpressionNode expression() {
        //Creates an expression node. Since we dont know if it will be a 
        //variable, operations,etc
        ExpressionNode answer;
        System.out.println("expression");
        //Can return a variable, or operation with mulop
        answer = simple_expression();
        
        if (currentToken == PascalToken.EQUALS
                || currentToken == PascalToken.NOTEQUAL
                || currentToken == PascalToken.LESSTHAN
                || currentToken == PascalToken.LESSTHANEQUAL
                || currentToken == PascalToken.GREATERTHANEQUAL
                || currentToken == PascalToken.GREATERTHAN) {
           //Create an operation for an expression
           OperationNode node = new OperationNode();
           node.setLeft(answer);        //sets the left expression
           node.setOperation(relop());  //sets the operation
           answer = simple_expression();//sends back a value, expression
           node.setRight(answer);       //Sets the right expression
           answer = node;               //operation complete 
        }
        
        return answer;
    }

    /**
     * parses for a relop symbol in the hash table then matches it to its token.
     * Will return a token for an operation node
     * @return 
     */
    private PascalToken relop() {
        //Creates a pascal token to send back to an operation node
        PascalToken answer= null;
        System.out.println("relop");

        if (currentToken == PascalToken.EQUALS) {
            answer = currentToken;  //Set token
            match(PascalToken.EQUALS);

        } else if (currentToken == PascalToken.NOTEQUAL) {
            answer = currentToken;  //set token
            match(PascalToken.NOTEQUAL);

        } else if (currentToken == PascalToken.LESSTHAN) {
            answer = currentToken;  //set token
            match(PascalToken.LESSTHAN);

        } else if (currentToken == PascalToken.LESSTHANEQUAL) {
            answer = currentToken;  //set token
            match(PascalToken.LESSTHANEQUAL);

        } else if (currentToken == PascalToken.GREATERTHANEQUAL) {
            answer = currentToken;  //set token
            match(PascalToken.GREATERTHANEQUAL);

        } else if (currentToken == PascalToken.GREATERTHAN) {
            answer = currentToken;  //set token
            match(PascalToken.GREATERTHAN);

        } else {
            error();

        }
        return answer;

    }

    /**
     * Simple expression parses for a plus or minus sign if either is present it
     * matches it to its token then run term and simple part whether a sign is
     * present or not.
     * @return 
     */
    private ExpressionNode simple_expression() {
        //Creates an expression node
        ExpressionNode answer = null;
        System.out.println("Simple_expression");
        if (currentToken == PascalToken.PLUS
                || currentToken == PascalToken.MINUS) {
            sign();
        }
        answer = term(answer); //Checkes for a Mulop
        answer = simple_part(answer);   //checks for addition, minus
        
        return answer;
    }

    /**
     * Parses for a minus or plus sign and matches it. If none are present then
     * the program quits with a warning.
     */
    private void sign() {

        System.out.println("sign");

        if (currentToken == PascalToken.PLUS) {
            match(PascalToken.PLUS);
        } else if (currentToken == PascalToken.MINUS) {
            match(PascalToken.MINUS);
        } else {
            System.out.println("Error in sign. Saw " + currentToken);
            error();
        }
    }

    /**
     * Parses to check if an addop is in place if there is not the method does
     * nothing at all.
     * @param node
     * @return 
     */
    private ExpressionNode simple_part(ExpressionNode node) {

        System.out.println("simple_part");

        if (currentToken == PascalToken.PLUS
                || currentToken == PascalToken.MINUS
                || currentToken == PascalToken.OR) {
            //Creates a simple operation node
            OperationNode answer = new OperationNode();
            answer.setLeft(node);       //sets the left expression
            answer.setOperation(addop());//Set the operation type
            answer.setRight(term(node));//sets the right expression
            simple_part(answer);        //Checks for more expression
            node = answer;
        }
        return node;
    }

    /**
     * Parses to check if an Mulop is in place if there is not the method does
     * nothing at all.
     * @param node
     * @return 
     */
    private ExpressionNode term_part(ExpressionNode node) {
        System.out.println("term_part");

        if (currentToken == PascalToken.MULTIPLY
                || currentToken == PascalToken.DIVIDE
                || currentToken == PascalToken.DIV
                || currentToken == PascalToken.MOD
                || currentToken == PascalToken.AND) {
            //Creats an mulop operation node
            OperationNode answer = new OperationNode();
            answer.setLeft(node);       //sets the left expression
            answer.setOperation(mulop());//sets the operation type
            answer.setRight(factor());  //sets the right expression
            term_part(answer);          //checks for more expressions
            node = answer;
        }
        return node;
    }

    /**
     * Parses for an ID to match and checks to see if there is an expression in
     * the ID. ID[expression].
     * @return VariableNode
     */
    private VariableNode variable() {
        //Creates a variable node to return
        VariableNode answer = new VariableNode();
        
        System.out.println("variable");
        //Sets the name of the variable
        answer.setName(scanner.getAttribute());        
        match(PascalToken.ID);
        if (currentToken == PascalToken.LEFT_BR) {
            match(PascalToken.LEFT_BR);
            expression();
            match(PascalToken.RIGHT_BR);
        }
        return answer;
    }

    /**
     * parses a declaration. It matches for a Variable and if there is no 
     * variables to parse the method does nothing.
     * @return  DeclarationsNode
     */
    private DeclarationsNode declarations() {
        System.out.println("declarations");
        //Creates a declarations node
        DeclarationsNode answer = new DeclarationsNode();

        //Part of the symbol table. 
        //Clears all the IDs from the previous declarations.
        idVerification.clear();

        if (currentToken == PascalToken.VAR) {
            match(PascalToken.VAR);
            //This calls the method idetifier_list to parse the variable(s).
            identifier_list();
            match(PascalToken.COLON);
            type();
            match(PascalToken.SEMICOLON);
            declarations();
        }
        //It trasfers the arraylist of variables from idetifier_list to
        //a new arraylist in declarations node. 
        answer.addAllArrayVars(varNode);
        return answer;

    }

    /**
     * Parses a type. Check to see if its an array or a standard type(integer or
     * real).
     */
    private void type() {
        System.out.println("type");

        if (currentToken == PascalToken.ARRAY) {
            //Overwrites the value of the hashtable in the symbol table
            //if it is an array. Since Keys are unique in a hashtable 
            //I just need to add the value type and the table is overwriten.
            for (String aVariableName : idVerification) {
                mySymbolTable.add(aVariableName, IdInfoToken.ARRAY_NAME);
            }
            match(PascalToken.ARRAY);
            match(PascalToken.LEFT_BR);
            match(PascalToken.NUMBER);
            match(PascalToken.COLON);
            match(PascalToken.NUMBER);
            match(PascalToken.RIGHT_BR);
            match(PascalToken.OF);
        }
        standard_type();
    }

    /**
     * Matches the type for a real or integer.
     */
    private void standard_type() {
        System.out.println("standard_type");

        if (currentToken == PascalToken.REAL) {
            match(PascalToken.REAL);

        } else if (currentToken == PascalToken.INTEGER) {
            match(PascalToken.INTEGER);
        }
    }

    /**
     * Parses a Subprogram declarations. Matches a Procedure or a function.
     */
    private void subprogram_declarations() {
        System.out.println("subprogram_declarations");

        if (currentToken == PascalToken.PROCEDURE 
                || currentToken == PascalToken.FUNCTION) {
            subprogram_declaration();
            match(PascalToken.SEMICOLON);
            subprogram_declarations();
        }

    }

    /**
     * Parses a Subprogram declaration. A subprogram is a program unit/module
     * that performs a particular task.
     */
    private void subprogram_declaration() {
        System.out.println("subprogram_declaration");
        subprogram_head();
        declarations();
        subprogram_declarations();
        compound_statement();
    }

    /**
     * Parses a Subprogram head. Will match a function or Procedure.
     */
    private void subprogram_head() {

        System.out.println("subprogram_head");

        if (currentToken == PascalToken.FUNCTION) {
            match(PascalToken.FUNCTION);
            //Adds a function to the symbol table and name of the function
            mySymbolTable.add(scanner.getAttribute(), IdInfoToken.FUNCTION_NAME);
            match(PascalToken.ID);
            arguments();
            match(PascalToken.COLON);
            standard_type();
            match(PascalToken.SEMICOLON);
        } else if (currentToken == PascalToken.PROCEDURE) {
            match(PascalToken.PROCEDURE);
            //Adds a procedure to the symbol table and name
            mySymbolTable.add(scanner.getAttribute(), IdInfoToken.PROCEDURE_NAME);
            match(PascalToken.ID);
            arguments();
            match(PascalToken.SEMICOLON);
        }

    }

    /**
     * Parses an arguments. Matches for - "(parameter list)".
     */
    private void arguments() {

        System.out.println("arguments");

        if (currentToken == PascalToken.LEFT) {
            match(PascalToken.LEFT);
            parameter_list();
            match(PascalToken.RIGHT);
        }
    }

    /**
     * Parses a Parameter list. It matches for - identifier_list:type |
     * identefier_list:type; parameter_list
     */
    private void parameter_list() {

        System.out.println("parameter_list");

        identifier_list();
        match(PascalToken.COLON);
        type();
        if (currentToken == PascalToken.SEMICOLON) {
            match(PascalToken.SEMICOLON);
            parameter_list();
        }

    }

    /**
     * Parses an expression. Implements exp -> term { addop term }
     */
    private PascalToken addop() {
        PascalToken answer = null;
        System.out.println("addop");
        if (currentToken == PascalToken.MINUS) {
            answer = currentToken;
            match(PascalToken.MINUS);
        } else if (currentToken == PascalToken.PLUS) {
            answer = currentToken;
            match(PascalToken.PLUS);
        } else if (currentToken == PascalToken.OR) {
            answer = currentToken;
            match(PascalToken.OR);
        } else {
            error();
        }
        return answer;
    }

    /**
     * Parses a term. Implements term -> factor { mulop factor }
     * @param answer
     * @return ExpressionNode
     */
    private ExpressionNode term(ExpressionNode answer) {
        //ExpressionNode answer;
        System.out.println("term");
        //returns the name of the variable
        answer = factor();
        //Checks for what fallows after the variable. 
        answer = term_part(answer);
        return answer;
    }

    /**
     * Parses an Expression. Implements exp -> term { mulop term }
     * @return PascalToken
     */
    private PascalToken mulop() {
        System.out.println("mulop");
        //Creates a pascal token.
        PascalToken answer = null;
        
        if (currentToken == PascalToken.DIVIDE) {
            answer = currentToken;  //sets token
            match(PascalToken.DIVIDE);
        } else if (currentToken == PascalToken.MULTIPLY) {
            answer = currentToken;  //sets token
            match(PascalToken.MULTIPLY);

        } else if (currentToken == PascalToken.AND) {
            answer = currentToken;  //sets token
            match(PascalToken.AND);

        } else if (currentToken == PascalToken.DIV) {
            answer = currentToken;  //sets token
            match(PascalToken.DIV);

        } else if (currentToken == PascalToken.MOD) {
            answer = currentToken;  //sets token
            match(PascalToken.MOD);
        }
        
        return answer;
    }

    /**
     * Parses a factor. Implements factor -> ( exp ) | Number| Not |ID |ID[exp]
     * |ID (exp).
     * @return ExpressionNode
     */
    private ExpressionNode factor() {
        System.out.println("factor");
        //Creates an expression node to return
        ExpressionNode answer=null;

        //This Id can be a function, array, or just a variable.
        //So the symbol table is used to check what type of Id it is
        if (currentToken == PascalToken.ID) {
            //Creates value node
            ValueNode node = new ValueNode();
            //Sets the name of the value node
            node.setAttribute(scanner.getAttribute());
            //Checks in the symbol table what type of Id it is.
            IdInfoToken token = mySymbolTable.getKind(scanner.getAttribute());
            match(PascalToken.ID);
            answer = node;  //sets the type of node
            //This will run if the Id is an array name
            if (token == IdInfoToken.ARRAY_NAME) {
                System.out.println("This is an array************");
                match(PascalToken.LEFT_BR);
                answer = expression();
                match(PascalToken.RIGHT_BR);

            //This will run if the Id is a funcion name
            } else if (token == IdInfoToken.FUNCTION_NAME) {
                System.out.println("This is an faunction************");

                match(PascalToken.LEFT);
                expression_list();
                match(PascalToken.RIGHT);
            }

        } else if (currentToken == PascalToken.NUMBER) {
            //Creates a value node 
            ValueNode node = new ValueNode();
            node.setAttribute(scanner.getAttribute());  //sets the value in the node
            match(PascalToken.NUMBER);
            answer = node;  //sets the node to return

        } else if (currentToken == PascalToken.LEFT) {
            match(PascalToken.LEFT);
            answer = expression();  //Sets an expression for an operation for if,while
            match(PascalToken.RIGHT);

        } else if (currentToken == PascalToken.NOT) {
            match(PascalToken.NOT);
            factor();
        } else {
            System.out.println("Error in Factor. Saw " + currentToken);
            error();
        }
        return answer;
    }

    /**
     * Matches a token. Matches the given token against the current token. Loads
     * the next token from the input file into the current token.
     *
     * @param expectedToken The token to match.
     */
    private void match(PascalToken expectedToken) {
        System.out.println("match " 
                + expectedToken 
                + " with current " 
                + currentToken 
                + ":" 
                + scanner.getAttribute());
        
        if (currentToken == expectedToken) {
            int scanResult = scanner.nextToken();
            switch (scanResult) {
                case PascalScanner.TOKEN_AVAILABLE:
                    currentToken = scanner.getToken();
                    break;
                case PascalScanner.TOKEN_NOT_AVAILABLE: // Handle no token case here
                    break;
                case PascalScanner.INPUT_COMPLETE: // Handle done case here
                    break;
            }
        } else {
            System.out.println("Error in match. Saw " + currentToken);
            error();  // We don't match!
        }
    }

    /**
     * Handles an error. Prints out the existence of an error and then exits.
     */
    private void error() {
        System.out.println("Error");
        System.exit(1);
    }

    /**
     * prints out an error massage for undeclared IDs.
     *
     * @param name
     */
    private void undeclaredId(String name) {
        System.out.println(
            "Undeclared statement, " + "'" + name + "'" + " cannot be found.");
    }
}