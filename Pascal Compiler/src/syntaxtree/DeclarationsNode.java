/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package syntaxtree;

import java.util.ArrayList;

/**
 *This class will hold all the variable declarations in an array list of 
 * Variable nodes. 
 * @author Henry
 */
public class DeclarationsNode extends SyntaxTreeNode{
    
    //Holds all the Variable nodes in an array list.
    //The array cannot be mutated in the class but the 
    //user is allowed to add to the array list but not delete
    // or change.
    final ArrayList<VariableNode> vars = new ArrayList();

    /**
     * Will return an array list of Variable nodes.
     * @return 
     */
    public ArrayList<VariableNode> getVars() {return vars;}
    
    /**Adds a variable node to the array list
     * @param vars 
     */
    public void addVars(VariableNode vars) {
        this.vars.add(vars);
    }
    /**
     * Will take all the Variable nodes in an array list.
     * This uses the .addAll(ArrayList<VariableNode nodes>)
     * method from the array List class.  
     * @param vars 
     */
    public void addAllArrayVars(ArrayList<VariableNode> vars){
        this.vars.addAll(vars);
    }
    
    /**
     * Does the indented toString of the tree, 
     * @param level
     * @return returns the node level of the tree of type IF
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Declarations: " +"\n";
        //Prints out the arraylist
        for(VariableNode state: vars){
            answer+= state.indentedToString(level+1);
        }
        
        return answer;
    }
}