/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package syntaxtree;

/**
 *This class will hold the structure of the statement node.
 * It will hold a Variable Node and an expression node.
 * Form: var bar := foo
 * Form: var bar := foo [operation] number
 * @author Henry
 */
public class AssignmentStatementNode extends StatementNode {
    private VariableNode lvalue; //Holds the Variable
    private ExpressionNode expression; //Holds the Expression
    
    
    //Settters
    /**
     * This method will store a variable Node in the class. 
     * @param lvalue 
     */
    public void setLvalue(VariableNode lvalue) {this.lvalue = lvalue;}
    /**
     * This method will store an expression node in the class. 
     * @param expression 
     */
    public void setExpression(ExpressionNode expression) {this.expression = expression;}
    
    
    //getters
    /**
     * This method will return the Variable node from the class.
     * @return 
     */
    public VariableNode getLvalue() {return lvalue;}
    /**
     * This method will return the expression node from the class. 
     * @return 
     */
    public ExpressionNode getExpression() {return expression;}
    
    /**
     * Does the indented toString of the tree, 
     * @param level
     * @return returns the node level of the tree of type IF
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "AssignmentStatementNode: " + "\n";
        answer += lvalue.indentedToString(level+1);
        answer += expression.indentedToString(level+1);
        
        return answer;
    }
}
