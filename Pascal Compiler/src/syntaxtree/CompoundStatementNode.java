/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package syntaxtree;

import java.util.ArrayList;

/**
 *This class will be holding all the Assignment statement node
 * @author Henry
 */
public class CompoundStatementNode extends StatementNode{
    //Holds all the Assignment statement nodes in an array list
    //The array has been declared as final so it cannot be 
    //changes or overwritten. We can add but not delete. 
    final ArrayList<StatementNode> statements = new ArrayList();
    
    /**
     * This method returns all the statements Nodes in the compound statement 
     * class. The return will be an array list. 
     * @return 
     */
    public ArrayList<StatementNode> getStatements() {return statements;}

    /**Adds an instance of a statements node to the array list
     * @param statements 
     */
    public void addStatement(StatementNode statements) {
        this.statements.add(statements);
    }
    
    /**
     * Will take all the Statement nodes in an array list.
     * This uses the .addAll(ArrayList<statement nodes>)
     * method from the array List class. 
     * @param statements 
     */
    public void addAllArrayList(ArrayList<StatementNode> statements){
        this.statements.addAll(statements);
    }
    
    /**
     * Does the indented toString of the tree, 
     * @param level
     * @return returns the node level of the tree of type IF
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Compound Statement: "  + "\n";
        //prints out the array list
        for(StatementNode state: statements){
           answer+= state.indentedToString(level+1);
        }
            
        return answer;
    }
}
