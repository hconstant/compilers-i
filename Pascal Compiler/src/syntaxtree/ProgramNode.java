/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntaxtree;
/**
 * This is the head node of the syntax tree.
 * This class holds all of the subPrograms, compound statements and 
 * program name. 
 * @author Henry
 */
public class ProgramNode extends SyntaxTreeNode {
    /**The name of the pascal program name **/
    private String name;
    /**This will hold all of the global declarations**/
    private DeclarationsNode variables;
    /**This will hold all of the subDeclarations*/
    private SubProgramDeclarationsNode functions;
    /**This will hold all of the compound statements*/
    private CompoundStatementNode main;

    
    //Setters 
    /**
     * Will set the name of the pascal program.
     * @param name 
     */
    public void setName(String name) 
    {this.name = name;}
    /**
     * Will set the global variable declarations of the pascal program.
     * @param variables 
     */
    public void setVariables(DeclarationsNode variables)
    {this.variables = variables;}
    
    /**
     *Will set the Subprogram declarations of the pascal program
     * @param functions 
     */
    public void setFunctions(SubProgramDeclarationsNode functions) 
    {this.functions = functions;}
    
    /**
     * Will set the main compound statements of the pascal program.
     * @param main 
     */
    public void setMain(CompoundStatementNode main) 
    {this.main = main;}

    
     //Getters
    /**
     * Will retrive the name of the pascal program.
     * @return 
     */
    public String getName() {return name;}
    /**
     * Will retrive the global variable declarations of the Pascal program.
     * @return 
     */
    public DeclarationsNode getVariables() {return variables;}
    /**
     * Will retrive the subdeclarations of the pascal program.
     * @return 
     */
    public SubProgramDeclarationsNode getFunctions() {return functions;}
    /**
     * Will retrive the compound statements of the main body of the pascal program.
     * @return 
     */
    public CompoundStatementNode getMain() {return main;}

    /**
     * Does the indented toString of the tree, 
     * @param level
     * @return returns the node level of the tree of type IF
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Program Name: " + this.name + "\n";
        answer += variables.indentedToString(level+1);
        answer += functions.indentedToString(level+1);
        answer += main.indentedToString(level+1);
        return answer;
    }
}
