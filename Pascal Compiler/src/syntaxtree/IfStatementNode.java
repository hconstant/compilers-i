/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package syntaxtree;

/**
 *This class is to create an if statement node.
 * It will hold the both statements from if(statement)
 * and else(statement) in a compound statement for expansion.
 * @author Henry
 */
public class IfStatementNode extends StatementNode {
    //Holds the if statement arguments and cannot be mutated 
    //but can be added to. 
    final CompoundStatementNode statements_1 = new CompoundStatementNode();
    //Holds the else statement arguments and cannot be mutated but can be
    // added to. 
    final CompoundStatementNode statements_2 = new CompoundStatementNode();
    //Will hold the expression 
    private ExpressionNode expression;

    //Setters
    /**
     * Will retrive the Compound statements of the if(statements)
     * @return 
     */
    public CompoundStatementNode getStatements_1() {
        return statements_1;
    }

    /**
     * Will retrive the compound statements of the else(statements)
     * @return 
     */
    public CompoundStatementNode getStatements_2() {
        return statements_2;
    }

    /**
     * Will retrive the expression node of the if(expression)
     * @return 
     */
    public ExpressionNode getExpresion() {
        return expression;
    }
    /**
     * Will set the expression of the if 
     * @param expression 
     */
    public void setExpression(ExpressionNode expression){
        this.expression = expression;
    }
    /**
     * Will add a statement node in the if(statements)
     * @param statement 
     */
    public void addIfStatement(StatementNode statement){
        this.statements_1.addStatement(statement);
    }
    
    /**
     * Will add a statement node in the Else(statement)
     * @param statement 
     */
    public void addElseStatements(StatementNode statement){
        this.statements_2.addStatement(statement);
    }
    
    /**
     * Does the indented toString of the tree, 
     * @param level
     * @return returns the node level of the tree of type IF
     */
    @Override
    public String indentedToString( int level)
    {
        String answer = super.indentedToString(level);
        answer += "If Statement " + "\n";
        answer += this.expression.indentedToString(level+1);
        answer += statements_1.indentedToString(level+1);
        answer += statements_2.indentedToString(level+1);
        
        return answer;
    } 
    
}
