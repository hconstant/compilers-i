/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package syntaxtree;

/**
 *This class will store the variable information
 * @author Henry
 */
public class VariableNode extends ExpressionNode {
    private String name; //variable name

    //getter
    /**
     * Will retrive the variable name in a string
     * @return 
     */
    public String getName() {return name;}

    //setter
    /**
     * Will set the variable name in a string.
     * @param name 
     */
    public void setName(String name) {this.name = name;}
    
    /**
     * Will print generic string name of the variable. 
     * @return 
     */
    @Override
    public String toString(){
        return name;
    }
    
    /**
     * Does the indented toString of the tree, 
     * @param level
     * @return returns the node level of the tree of type IF
     */
     @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Variable Node: " + name + "\n";
        return answer;
    }
}
