/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package syntaxtree;

/**
 *This class will create a while node for the pascal language.
 * @author Henry
 */
public class WhileStatementNode extends StatementNode {
    //Will store the expression of the while loop
    private ExpressionNode expression;
    //Will store the statements in the while loop
    final CompoundStatementNode statements = new CompoundStatementNode();
    /**
     * Will return the statements in the while loop.
     * @return 
     */
    public StatementNode getStatement(){
        return this.statements;
    }
    /**
     * Will store a statement into compoundStatementNode that is in a while loop.
     * @param statement 
     */
    public void addWhileStatement(StatementNode statement){
        this.statements.addStatement(statement);
    }
    /**
     * Will Set the expression that is given to the while loop.
     * @param expression 
     */
    public void setExpression(ExpressionNode expression){
        this.expression = expression;
    }
    /**
     * Will retrive the expression of the while loop. 
     * @return 
     */
    public ExpressionNode getExpression(){
        return this.expression;
    }
    
    /**
     * Does the indented toString of the tree, 
     * @param level
     * @return returns the node level of the tree of type IF
     */
    @Override
    public String indentedToString( int level)
    {
        String answer = super.indentedToString(level);
        answer += "While Statement " + "\n";
        answer += this.expression.indentedToString(level+1);
        answer += statements.indentedToString(level+1);
      
        
        return answer;
    } 
}
