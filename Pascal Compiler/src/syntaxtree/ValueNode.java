/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package syntaxtree;

/**
 *This class stores the value of the variable expression. 
 * @author Henry
 */
public class ValueNode extends ExpressionNode {
    
    //Will hold the value
    private String attribute;
    
    //getters
    /**
     * Will retrive the attribute value 
     * @return 
     */
    public String getAttribute( ) {return attribute;}
    
    //setters
    /**
     * Will set the attribute value.
     * @param attribute 
     */
    public void setAttribute( String attribute) {this.attribute = attribute;}
    
    /**
     * Does the indented toString of the tree, 
     * @param level
     * @return returns the node level of the tree of type IF
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "ValueNode: "  + attribute + "\n";
        return answer;
    }
    
}
