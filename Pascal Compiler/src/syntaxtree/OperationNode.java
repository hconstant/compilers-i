/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package syntaxtree;
import scanner.PascalToken;

/**
 *This class will create an operation node which extends from expression.
 * @author Henry
 */
public class OperationNode extends ExpressionNode{
    private PascalToken operation;  //Stores the operation type: -, +, /, division 
    private ExpressionNode left;    //Stores the left expression
    private ExpressionNode right;   //stores the right expression of the equation
    
    //getters
    /**
     * This will retrive the operation type from the expression.
     * +, -, *, divide, etc
     * @return 
     */
    public PascalToken getOperation() {return operation;}
    /**
     * This will retrive the left side of the expression.
     * @return 
     */
    public ExpressionNode getLeft() {return left;}
    /**
     * This will retrive the right side of the expression.
     * @return 
     */
    public ExpressionNode getRight() {return right;}
    
    //setters
    /**
     * Will set the operation type of the expression 
     * +,-, divide, multiplication, etc
     * @param operation 
     */
    public void setOperation(PascalToken operation) {this.operation = operation;}
    /**
     * Will set the left side of the expression.
     * @param left 
     */
    public void setLeft(ExpressionNode left) {this.left = left;}
    /**
     * Will set the right side of the expression.
     * @param right 
     */
    public void setRight(ExpressionNode right) {this.right = right;}
    
    /**
     * Does the indented toString of the tree, 
     * @param level
     * @return returns the node level of the tree of type IF
     */
     @Override
    public String indentedToString(int level) {
        String answer = super.indentedToString(level);
        answer += "Operation Node: "  + operation + "\n";
        answer += left.indentedToString(level+1);
        answer += right.indentedToString(level+1);   
        return answer;
    }
    
}
