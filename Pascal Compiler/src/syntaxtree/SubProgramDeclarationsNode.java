/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package syntaxtree;

import java.util.ArrayList;

/**
 *This class will store all the subprogram declarations of the pascal program.
 * @author Henry
 */
public class SubProgramDeclarationsNode extends SyntaxTreeNode{
    //Will store all the variable nodes. Cannot be mutated but can be added.
    final ArrayList<VariableNode> procs = new ArrayList();
    
    /**Adds a procedure node to the array list
     * @param procs
     */
    public void addProcs(VariableNode procs){
        this.procs.add(procs);
    }
    
    /**
     * Does the indented toString of the tree, 
     * @param level
     * @return returns the node level of the tree of type IF
     */
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "SubPrograms Declarations:" + "\n";
        //Prints out the array list
        for(VariableNode state: procs){
            answer+= state.indentedToString(level+1);
        }
        return answer;
    }
}
