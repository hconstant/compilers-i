/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package scanner;

import java.util.Hashtable;

/**
 *This is the reserved words look up table for the pascal language. 
 * @author Henry Constantino
 */
public class LookupTable extends Hashtable {
    
    public LookupTable () {
        super();
                this.put("if", PascalToken.IF);
		this.put("while", PascalToken.WHILE);
                this.put("read", PascalToken.READ);
                this.put("write", PascalToken.WRITE);
		this.put("begin", PascalToken.BEGIN);
		this.put("and", PascalToken.AND);
		this.put("program", PascalToken.PROGRAM);
		this.put("do", PascalToken.DO);
		this.put("procedure", PascalToken.PROCEDURE);
		this.put("mod", PascalToken.MOD);
		this.put("integer", PascalToken.INTEGER);
		this.put("else", PascalToken.ELSE);
		this.put("function", PascalToken.FUNCTION);
		this.put("not", PascalToken.NOT);
		this.put("letter", PascalToken.LETTER);
		this.put("real", PascalToken.REAL);
		this.put("end", PascalToken.END);
		this.put("or", PascalToken.OR);
		this.put("then", PascalToken.THEN);
		this.put("array", PascalToken.ARRAY);
		this.put("var", PascalToken.VAR);
		this.put("of", PascalToken.OF);
		//Symbols for the Pascal
		this.put("div",PascalToken.DIV);
		this.put(";", PascalToken.SEMICOLON);
		this.put(">=",PascalToken.GREATERTHANEQUAL);
		this.put(":=",PascalToken.ASSIGNOP);
		this.put("=", PascalToken.EQUALS);
		this.put("+", PascalToken.PLUS);
		this.put("[", PascalToken.LEFT_BR);
		this.put("<", PascalToken.LESSTHAN);
		this.put("-", PascalToken.MINUS);
		this.put("]", PascalToken.RIGHT_BR);
		this.put(":", PascalToken.COLON);
		this.put(">", PascalToken.GREATERTHAN);
		this.put("*", PascalToken.MULTIPLY);
		this.put("(", PascalToken.LEFT);
		this.put(".", PascalToken.PERIOD);
		this.put("<=",PascalToken.LESSTHANEQUAL);
		this.put("<>", PascalToken.NOTEQUAL);
		this.put("/", PascalToken.DIVIDE);
		this.put(")", PascalToken.RIGHT);
		this.put("{", PascalToken.LEFT_CURLY);
		this.put("}", PascalToken.RIGHT_CURLY);
		this.put(",", PascalToken.COMMA);

    }
}
