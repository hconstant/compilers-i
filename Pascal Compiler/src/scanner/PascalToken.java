/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package scanner;

/**
 *This is the reserved words that are in the pascal language. 
 * @author Henry Constantino
 */
public enum PascalToken {
	
	NUMBER, IF, WHILE, BEGIN, AND, PROGRAM, DO, PROCEDURE, MOD, INTEGER,ID,
        OF,ELSE, FUNCTION, NOT, LETTER, REAL, END, OR, THEN, ARRAY, VAR, 
        ASSIGNOP,DIV, SEMICOLON, GREATERTHANEQUAL, EQUALS, PLUS,LEFT_BR, 
        LESSTHAN, MINUS, RIGHT_BR, COLON, GREATERTHAN, MULTIPLY,LEFT, PERIOD, 
        LESSTHANEQUAL, DIVIDE, RIGHT, LEFT_CURLY, RIGHT_CURLY,NOTEQUAL,COMMA,
        READ, WRITE
}
