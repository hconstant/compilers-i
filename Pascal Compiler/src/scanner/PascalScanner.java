
package scanner;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.util.Hashtable;

public class PascalScanner {
	
	//Internal States of the transition table
    private static final int START_STATE = 0;
    private static final int IN_NUMBER_STATE = 1;
    private static final int IN_ID_STATE = 2;
    private static final int IN_LESSTHAN_STATE = 3;
    private static final int IN_GREATERTHAN_STATE = 4;
    private static final int IN_COLON_STATE = 5;
    private static final int COMMENTS_STATE = 6;
    
    //Final states of the transition table
    private static final int ERROR_STATE = 50;
    private static final int SYMBOL_COMPLETE_STATE = 51;
    private static final int NUMBER_COMPLETE_STATE = 52;
    private static final int ID_COMPLETE_STATE = 53;
    private static final int LESSTHAN_EQUAL_COMPLETE_STATE = 54;
    private static final int NOT_EQUAL_COMPLETE_STATE = 55;
    private static final int LESSTHAN_COMPLETE_STATE = 56;
    private static final int GREATERTHAN_COMPLETE_STATE = 57;
    private static final int GREATERTHAN_EQUAL_COMPLETE_STATE = 58;
    private static final int ASSIGNOP_COMPLETE_STATE = 59;
    private static final int FUNCTION_DECLARATION_COMPLETE_STATE = 60;
    
    public static final int TOKEN_AVAILABLE = 0;
    public static final int TOKEN_NOT_AVAILABLE = 1;
    public static final int INPUT_COMPLETE = 2;

    private int[][] transitionTable;  // table[state][char]
    private PascalToken token;
    private String attribute;
    private PushbackReader inputReader;
    private Hashtable symbolTable;

    /**
     * Takes in the Syntax file to be Scanned
     * @param inputFile
     * @param symbolTable
     */
    public PascalScanner( File inputFile, Hashtable symbolTable) {
        this.symbolTable = symbolTable;		//Hash Symbol table
        try {
            this.inputReader = new PushbackReader( new FileReader(inputFile));
        }
        catch( Exception e) {
            e.printStackTrace();
            System.exit( 1);
        }
        this.transitionTable = createTransitionTable(); 
    }
    
    public String getAttribute() { return( this.attribute);}
    public PascalToken getToken() { return( this.token);}
    
    public int nextToken() {
        int currentState = START_STATE;
        StringBuilder lexeme = new StringBuilder("");
        while( currentState < ERROR_STATE ) {
            char currentChar = '\0';
            try {
                currentChar = (char) inputReader.read();
            }
            catch( Exception e) {
                // End of stream here.
                System.out.println("Should return input complete");
                return( INPUT_COMPLETE);
            }
           // System.out.println("current char is actually " + (int)currentChar);
            
            //Quits the program when it reaches the end of the file.
            if( currentChar == 65535) {
            	if(currentState == 0)
            	{
            		return (INPUT_COMPLETE);
            	}
            	else {
            		currentChar = 10;	//Creates a new line for the scanner to quit
            	}
            }
            
            if( currentChar > 127) return( TOKEN_NOT_AVAILABLE);
           //System.out.print("State:" + currentState + " Char: " + currentChar + " (" + (int)currentChar + ")");
            int nextState = transitionTable[currentState][currentChar];
           //System.out.println(" Next State: " + nextState + " current lexeme: ["+ lexeme.toString() + "]");
            switch( nextState) {
                // Start State
                case START_STATE:
                    lexeme = new StringBuilder("");
                    break;
                                    
                // find a number (an int at this point)
                case IN_NUMBER_STATE:
                    lexeme.append( currentChar);
                    break;
                    
                case NUMBER_COMPLETE_STATE:
                	// After a single symbol, just push out the single lexeme
                    try {
                        inputReader.unread(currentChar);
                    }
                    catch( IOException ioe) {
                    	ioe.printStackTrace();}
                    
                    token = PascalToken.NUMBER;
                    attribute = lexeme.toString();
                    break;
                    
                case COMMENTS_STATE:
                	//Comments are completely ignored in the scanner
                	//Since it return back to 0 state with {comment} is seen.
                    lexeme.append( currentChar);
                    token = (PascalToken)symbolTable.get( lexeme.toString());
                    attribute = lexeme.toString();
                	break;
                	
                case IN_ID_STATE:
                    lexeme.append( currentChar);
                	break;
                	
                case ID_COMPLETE_STATE:
                	//Identifies an attribute as a variable ID
                	// After a single symbol, just push out the single lexeme
                	 try {
                         inputReader.unread(currentChar);
                     }
                     catch( IOException ioe) {
                    	 ioe.printStackTrace();
                    	 }
                	 
                     token = (PascalToken)symbolTable.get( lexeme.toString());
                     attribute = lexeme.toString();
                     
                     //Since and ID wont be in the look up hash table the
                     //token will be null. This prevents the scanner from 
                     //Seeing it as an error.
                     if(token == null){
                    	 token = PascalToken.ID;
                    	 attribute = lexeme.toString();
                     }
                     
                    break;
                	
                case SYMBOL_COMPLETE_STATE:
                	//Identifies a symbols from the transition table
                    // After a single symbol, just push out the single lexeme
                    lexeme.append( currentChar);
                    token = (PascalToken)symbolTable.get( lexeme.toString());
                    attribute = lexeme.toString();
                    break;
                    
                case IN_COLON_STATE:
                    lexeme.append( currentChar);
                	break;
                	
                case ASSIGNOP_COMPLETE_STATE:
                	//Identifies a assignop
                	// After a single symbol, just push out the single lexeme
                    lexeme.append( currentChar);
                	token = (PascalToken)symbolTable.get( lexeme.toString());
                    attribute = lexeme.toString();
                	break;
                	
                case FUNCTION_DECLARATION_COMPLETE_STATE:
                	//Identifies a function declaration symbol
                	try {
                        inputReader.unread(currentChar);
                    }
                    catch( IOException ioe) {
                   	 ioe.printStackTrace();
                   	 }
                	
                	// After a single symbol, just push out the single lexeme
                    token = (PascalToken)symbolTable.get( lexeme.toString());
                    attribute = lexeme.toString();
                	break;
                	
                case IN_GREATERTHAN_STATE:
                	lexeme.append( currentChar);
                	break;
                	
                case GREATERTHAN_COMPLETE_STATE:
                	//Identifies greater than symbol >
                	try {
                        inputReader.unread(currentChar);
                    }
                    catch( IOException ioe) {
                   	 ioe.printStackTrace();
                   	 }
                    // After a single symbol, just push out the single lexeme
                	token = (PascalToken)symbolTable.get( lexeme.toString());
                    attribute = lexeme.toString();
                	break;
                	
                case GREATERTHAN_EQUAL_COMPLETE_STATE:
                	//Identifies a greater than symbol >=
                	// After a single symbol, just push out the single lexeme
                    lexeme.append( currentChar);
                    token = (PascalToken)symbolTable.get( lexeme.toString());
                    attribute = lexeme.toString();
                	break;
                	
                case IN_LESSTHAN_STATE:
                	lexeme.append( currentChar);
                	break;
                	
                case LESSTHAN_EQUAL_COMPLETE_STATE:
                	//Identifies a lessthan equal symbol <=
                	// After a single symbol, just push out the single lexeme
                    lexeme.append( currentChar);
                    token = (PascalToken)symbolTable.get( lexeme.toString());
                    attribute = lexeme.toString();
                	break;
                
                case LESSTHAN_COMPLETE_STATE:
                	//Identifies a lessthan symbol <
                	try {
                        inputReader.unread(currentChar);
                    }
                    catch( IOException ioe) {
                   	 ioe.printStackTrace();
                   	 }
                	// After a single symbol, just push out the single lexeme
                    token = (PascalToken)symbolTable.get( lexeme.toString());
                    attribute = lexeme.toString();
                	break;
                	
                case NOT_EQUAL_COMPLETE_STATE:
                	//Identifies a not equal symbol <>
                	// After a single symbol, just push out the single lexeme
                    lexeme.append( currentChar);
                    token = (PascalToken)symbolTable.get( lexeme.toString());
                    attribute = lexeme.toString();
                	break;
                	
                case ERROR_STATE:
                	//Not in the transition table or lexeme
                    return( TOKEN_NOT_AVAILABLE);
                    
            } // end switch
            currentState = nextState;
        } // end while currentState <= ERROR_STATE
        return( TOKEN_AVAILABLE);
    }

    /**
     * Transition table for the Pascal compiler.
     * Internal states from 0 to 6.
     * 
     */
    private int[][] createTransitionTable() {
        return new int[][]
    {
        // State 0: Start
        {
           50,50,50,50,50,50,50,50,50, 0, 0,50,50, 0,50,50, //   0 -  15 (00-0f)
           50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50, //  16 -  31 (10-1f)
            0,50,50,50,50,50,50,50,51,51,51,51,51,51,51,51, //  32 -  47 (20-2f)
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5,51, 3,51, 4,50, //  48 -  63 (30-3f)
           50, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, //  64 -  79 (40-4f)
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,51,50,51,50,50, //  80 -  95 (50-5f)
           50, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, //  96 - 111 (60-6f)
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 6,50,50,50,50  // 112 - 127 (70-7f)
        },
        
        // State 1: In Number
        {
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //   0 -  15 (00-0f)
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //  16 -  31 (10-1f)
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //  32 -  47 (20-2f)
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,52,52,52,52,52,52, //  48 -  63 (30-3f)
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //  64 -  79 (40-4f)
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //  80 -  95 (50-5f)
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52, //  96 - 111 (60-6f)
           52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52  // 112 - 127 (70-7f)
        },
        
        // State 2: In Variable
        {
           53,53,53,53,53,53,53,53,53,53,53,53,53,53,53,53, //   0 -  15 (00-0f)
           53,53,53,53,53,53,53,53,53,53,53,53,53,53,53,53, //  16 -  31 (10-1f)
           53,53,53,53,53,53,53,53,53,53,53,53,53,53,53,53, //  32 -  47 (20-2f)
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2,53,53,53,53,53,53, //  48 -  63 (30-3f)
           53, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, //  64 -  79 (40-4f)
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,53,53,53,53,53, //  80 -  95 (50-5f)
           53, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, //  96 - 111 (60-6f)
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,53,53,53,53,53  // 112 - 127 (70-7f)
        },
        
        // State 3: IN LessThan
        {
           56,56,56,56,56,56,56,56,56,56,56,56,56,56,56,56, //   0 -  15 (00-0f)
           56,56,56,56,56,56,56,56,56,56,56,56,56,56,56,56, //  16 -  31 (10-1f)
           56,56,56,56,56,56,56,56,56,56,56,56,56,56,56,56, //  32 -  47 (20-2f)
           56,56,56,56,56,56,56,56,56,56,56,56,56,54,55,56, //  48 -  63 (30-3f)
           56,56,56,56,56,56,56,56,56,56,56,56,56,56,56,56, //  64 -  79 (40-4f)
           56,56,56,56,56,56,56,56,56,56,56,56,56,56,56,56, //  80 -  95 (50-5f)
           56,56,56,56,56,56,56,56,56,56,56,56,56,56,56,56, //  96 - 111 (60-6f)
           56,56,56,56,56,56,56,56,56,56,56,56,56,56,56,56  // 112 - 127 (70-7f)
        },
        
        // State 4: IN GreaterThan
        {
           57,57,57,57,57,57,57,57,57,57,57,57,57,57,57,57, //   0 -  15 (00-0f)
           57,57,57,57,57,57,57,57,57,57,57,57,57,57,57,57, //  16 -  31 (10-1f)
           57,57,57,57,57,57,57,57,57,57,57,57,57,57,57,57, //  32 -  47 (20-2f)
           57,57,57,57,57,57,57,57,57,57,57,57,57,58,57,57, //  48 -  63 (30-3f)
           57,57,57,57,57,57,57,57,57,57,57,57,57,57,57,57, //  64 -  79 (40-4f)
           57,57,57,57,57,57,57,57,57,57,57,57,57,57,57,57, //  80 -  95 (50-5f)
           57,57,57,57,57,57,57,57,57,57,57,57,57,57,57,57, //  96 - 111 (60-6f)
           57,57,57,57,57,57,57,57,57,57,57,57,57,57,57,57  // 112 - 127 (70-7f)
        },
        
   	 	// State 5: IN Colon
        {
           60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60, //   0 -  15 (00-0f)
           60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60, //  16 -  31 (10-1f)
           60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60, //  32 -  47 (20-2f)
           60,60,60,60,60,60,60,60,60,60,60,60,60,59,60,60, //  48 -  63 (30-3f)
           60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60, //  64 -  79 (40-4f)
           60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60, //  80 -  95 (50-5f)
           60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60, //  96 - 111 (60-6f)
           60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60  // 112 - 127 (70-7f)
        },
        
        // State 6: Comments
        {
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, //   0 -  15 (00-0f)
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, //  16 -  31 (10-1f)
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, //  32 -  47 (20-2f)
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, //  48 -  63 (30-3f)
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, //  64 -  79 (40-4f)
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, //  80 -  95 (50-5f)
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, //  96 - 111 (60-6f)
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 6, 6  // 112 - 127 (70-7f)
        },
    };
    }
}
