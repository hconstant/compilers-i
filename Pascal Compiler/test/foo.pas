{Testing Expressions and fuction and procedure}
program expressionTest;

{Expression}
var c : real;
{Expression with multiple variables}
var a, b, d, e, f : integer;

{Function declaration}
function divide (foobar : integer; shanna:real) : integer ;

{Compound Statement}
begin
end;

{Compound Statement}
begin

{Statements}
    a := 3;
    b := a * 4;
    c := (b + a)/ 2;
    d := c mod 10;
    e := 20 div 10; 
    f := c and b
end.