{Testing if statements, it should parse}
program ifTest;

{Expression with multiple variables}
var foo,bar:integer;
var b, a : integer;

{Compound Statement}
begin
{Statement}
	foo := 0;
	
	{If Else Statement}
	if foo = 0 then
	{Compound Statement}
		{Statements}
		bar := 4	
	else
		b := 6	
	{End of If Else}
end.