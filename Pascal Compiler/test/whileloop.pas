{Testing While do loop, Should be parse}
program whileLoop;

{Expression declaration}
var a: integer;
 
{Compound Statement} 
begin

{Statement}
   a := 10;
   {While do loop}
   while  a >= 20  do
     {Statement}
      a := a + 1
   {End of While do loop}
end.